<?php
	$settings = array(
		// 設定名 => デフォルト値,
		// 全部stringで！
		"useAjax" => "false",
		"useJapaneseInHeader" => "false",
		"useMonaFont" => "false",
		"theme" => "default",
		"useNightMode" => "false",
	);
	foreach ($settings as $key => $value) {
		if (!empty($_COOKIE[$key]) && isset($_COOKIE[$key])) {
			$settings[$key] = $_COOKIE[$key];
		}
		if (!empty($post2[$key]) && isset($post2[$key])) {
			if ($post2[$key] === "on") {
				$settings[$key] = "true";
				setcookie($key, "true");
			} elseif ($post2[$key] === "off") {
				$settings[$key] = "false";
				setcookie($key, "false");
			} else {
				$settings[$key] = $post2[$key];
				setcookie($key, $post2[$key]);
			}
		}
	}
	unset($key, $value);
?>
