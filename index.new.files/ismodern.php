<?php
	$browser_index_new_css = get_browser(null, true);
	switch ($browser_index_new_css["browser"]) {
		case "w3m":
		case "IE":
		case "3DS":
			echo "false";
			break;
		case "Nintendo Browser":
			if ($browser_index_new_css["device_name"] === "3DS") {
				echo "false";
			} else {
				echo "true";
			}
			break;
		case "Browser":
			if ($browser_index_new_css["parent"] === "w3m") {
				echo "false";
			} else {
				echo "true";
			}
			break;
		default:
			echo "true";  // ほんとぉ？
			break;
	}
	unset($browser_index_new_css);
?>
