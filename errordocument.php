<?php
	$array  = array('　 ∧＿∧
　（　´∀｀）
　（　　　　）
　｜ ｜　|
　（_＿）＿）'
, '　 ∧＿∧
　（　・∀・）
　（　　　　）
　｜ ｜ ｜
　（_＿）＿）', '

　　　＿＿＿_∧∧
　～\'　＿＿__(,,ﾟДﾟ)
　　 ＵU 　 　Ｕ U', '
　　　　∧ ∧＿__
　 　／(*ﾟーﾟ)　／＼
　／|￣∪∪￣|＼／
　 　|　 　 　 　 |／
　 　 ￣￣￣￣');
	$array2 = array('モナー', 'モララー', 'ギコ', 'しぃ');
	$array3 = array('about', 'settings', 'home', 'oldpage');
	$text = "さらにエラーが発生しました！ エラードキュメント内の配列数が一致しません！";
	if (count($array) === count($array2) && count($array) === count($array3)) {
		$num = mt_rand(0, count($array) - 1);
		$text = $array2[$num] . "\n" .  $array[$num];
		$genre = $array3[$num];
	}
	#genre="";

	$error_title_array = [
		200 => "OK!?!?",
		403 => "Forbidden",
		404 => "Not Found",
		500 => "Internal Server Error"
	];
	$error_title_array2 = [
		0 => "Unknown Reponce Code!",
		1 => "Info",
		2 => "Success",
		3 => "Redirect",
		4 => "Error",
		5 => "Server Error"
	];
	$error_desc_array = [
		200 => "成功です！？エラーページを直接参照したかも？",
		403 => "アクセスが拒否されました（サーバー側がおかしいかも）",
		404 => "ファイルまたはディレクトリが存在しないようです。",
		500 => "サーバー内部エラーが発生しました！"
	];
	$error_desc_array2 = [
		0 => "不明なレスポンスコードです！",
		1 => "情報レスポンスです！",
		2 => "成功レスポンスです！",
		3 => "リダイレクト中です！（多分）",
		4 => "クライアント エラーです！",
		5 => "サーバーがエラってます！"
	];

	if ($_SERVER['HTTP_X_STATUS'] === "000") {
		$responce_code = 200;
	} else {
		$responce_code = $_SERVER['HTTP_X_STATUS'] ?? http_response_code();
	}
	$error_title = $error_title_array[$responce_code] ?? $error_title_array2[mb_substr((string)$responce_code, 0, 1)] ?? $error_title_array2[0] ?? 'Error Page is bugging!!';
	$error_desc = $error_desc_array[$responce_code] ?? $error_desc_array2[mb_substr((string)$responce_code, 0, 1)] ?? $error_desc_array2[0] ?? 'エラーページがバグってます！！';
	echo <<<"EOD"
<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="/index.new.css">
		<title>$responce_code $error_title</title>
		<style>
			body {
				margin: 8px;
			}
			h1 {
				margin-left: 0;
			}
			.mona {
				font-size: 16px;
				line-height: 18px;
				font-family: 'Monapo', 'Mona', 'IPAMonaPGothic', 'IPA モナー Pゴシック', 'Togoshi Mona Gothic', '戸越モナーゴシック', 'MS PGothic', 'ＭＳ Ｐゴシック', sans-serif;
			}
		</style>
	</head>
	<body class="body-$genre">
		<h1>$responce_code $error_title</h1>
		<h2>$error_desc</h2>
		<hr>
		<pre class="mona">
$text
</pre>
	</body>
</html>

EOD;
?>
