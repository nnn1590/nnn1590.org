<?php
	$reloading = false;
	if (!empty($_POST['theme']) && isset($_POST['theme'])) {
		setcookie("theme", $_POST['theme']);
	}

	if ($reloading) {
		echo <<<'EOF'
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="refresh" content="0; URL=">
	</head>
	<body>
		設定しました！
		再読み込みしています…<br>
		<a href="/index.php">切り替わらない場合、ここをクリックしてください。</s>
	</body>
</html>
EOF;
		die();
	}
?>
<?php $title = "Top page - NNN1590.org"; $title_ja = "トップページ - NNN1590.org"; $page = "home"; $h1 = "Welcome to " . (($_SERVER['HTTP_HOST'] == "nnn1590.orz.hm") ? "NNN1590.orz.hm(NNN1590.org)!" : "NNN1590.org!"); $h1_ja = "ようこそ！"; include 'parts/base-mix.php'; ?>
			<script>
				// @license https://creativecommons.org/licenses/zero/1.0/ CC0-1.0
				function doSomething() {
					document.body.className = "body-" + ['home', 'news', 'links', 'about', 'settings', 'oldpage'][Math.floor(Math.random()*6)];
				}
				// @license-end
			</script>
<?php
	if (preg_match('/(?i)msie /', $_SERVER["HTTP_USER_AGENT"])) {
		echo <<<EOF
			<h2>もしかして：Internet Explorer ユーザー？</h2>
			<p>
				あなたはInternet Explorerを使ってアクセスしているようです（もしくはIEのユーザーエージェントにしているようです）。<br>
				クラシック表示にしますか？(Cookieが必要です)
				<form action="#" method="POST" enctype="multipart/form-data">
					テーマ: <select name="theme" id="select-theme">

EOF;
					$theme_array = [
						"default" => "デフォルト",
						"simplest" => "超シンプル",
						"simplest_mona" => "超シンプル - モナー",
						"simplest_dark" => "超シンプル(ダーク)",
						"simplest_dark_mona" => "超シンプル(ダーク) - モナー"
					];

					foreach ($theme_array as $theme_internal_name => $theme_display_name) {
					if (!empty($_POST['theme']) && isset($_POST['theme'])) {
						if ($_POST["theme"] === $theme_internal_name) {
							$add = " selected";
						} else {
							$add = "";
						}
					} else {
						if ($_COOKIE["theme"] === $theme_internal_name) {
							$add = " selected";
						} else {
							$add = "";
						}
					}
						echo "\t\t\t\t\t\t" . '<option value="' . $theme_internal_name . '"' . $add . '>' . $theme_display_name . '</option>' . "\n";
					}
			echo <<<EOF
					</select>
					<input type="submit" value="設定する">
				</form>
			</p>

EOF;		
	}
			?>
			<h2>トップページ</h2>
			<p>
				<a href="guess_the_hash.php">ハッシュ当てクイズ</a>
			</p>
			<button onclick="doSomething();">テスト</button><noscript>このボタンを使うにはJavaScriptを有効化してください。(LibreJSを利用していても表示されるかもしれません)</noscript>
		</div>
	</body>
</html>
