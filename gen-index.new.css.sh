#!/bin/bash
set -e

_CONFIG_OUTPUT="index.new.GENERATED.css"
rm -fv "${_CONFIG_OUTPUT}"

_CONFIG_COLORS=(
	#Name      R   G   B
	'home        0  64 255'
	'news      255  64   0'
	'links      64 192 255'
	'about       0 192   0'
	'settings  255 255  64'
	'oldpage   255  64 192'
)

_STRIPE_WIDTH_FG="10px"  # 12px
_STRIPE_WIDTH_BG="8px"  # 10px

# ===== Write header =====
cat > "${_CONFIG_OUTPUT}" << 'EOF'
/*
	NOTE: This css is auto-generated.
	License: CC0
	Generator source: /gen-index.new.css.sh
*/

EOF

# ===== Generate color defs =====
echo ':root {' >> "${_CONFIG_OUTPUT}"
for i in "${_CONFIG_COLORS[@]}"; do
	_data=(${i[@]})
	_name="${_data[0]}"
	_red="${_data[1]}"
	_green="${_data[2]}"
	_blue="${_data[3]}"
	# タブ考慮忘れずに
	cat >> "${_CONFIG_OUTPUT}" << EOF
	--top-navigation-${_name}-color-r: ${_red};
	--top-navigation-${_name}-color-g: ${_green};
	--top-navigation-${_name}-color-b: ${_blue};
	--top-navigation-${_name}-color: rgb(var(--top-navigation-${_name}-color-r), var(--top-navigation-${_name}-color-g), var(--top-navigation-${_name}-color-b));
	--top-navigation-${_name}-color-darker: rgba(var(--top-navigation-${_name}-color-r), var(--top-navigation-${_name}-color-g), var(--top-navigation-${_name}-color-b), calc(64/256));

EOF
done
sed -i -e '$d' "${_CONFIG_OUTPUT}"
echo '}' >> "${_CONFIG_OUTPUT}"
echo '' >> "${_CONFIG_OUTPUT}"

# ===== Generate top navs =====
for i in "${_CONFIG_COLORS[@]}"; do
	_data=(${i[@]})
	_name="${_data[0]}"
	# タブ考慮忘れずに
	cat >> "${_CONFIG_OUTPUT}" << EOF
.top-navigation-${_name} {
	background-image: repeating-linear-gradient(135deg, transparent, transparent ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_FG}), linear-gradient(180deg, black, rgba(var(--top-navigation-${_name}-color-r), var(--top-navigation-${_name}-color-g), var(--top-navigation-${_name}-color-b), calc(128/256)));
	border-bottom: 3px solid var(--top-navigation-${_name}-color);
}
.top-navigation-${_name}::after {
	background-image: repeating-linear-gradient(135deg, transparent, transparent ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_FG}), linear-gradient(180deg, black, rgba(var(--top-navigation-${_name}-color-r), var(--top-navigation-${_name}-color-g), var(--top-navigation-${_name}-color-b), calc(256/256)));
	border-bottom: 3px solid var(--top-navigation-${_name}-color);
}
.top-navigation-${_name} > .triangle {
	border-bottom-color: var(--top-navigation-${_name}-color)!important;
}
EOF
done
echo '' >> "${_CONFIG_OUTPUT}"

# ===== Generate body background =====
for i in "${_CONFIG_COLORS[@]}"; do
	_data=(${i[@]})
	_name="${_data[0]}"
	# タブ考慮忘れずに
	cat >> "${_CONFIG_OUTPUT}" << EOF
.body-${_name} {
	background-image: linear-gradient(180deg, rgba(0,0,0,0.9), rgba(0,0,0,0.5)), repeating-linear-gradient(135deg, transparent, transparent ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_BG}, var(--top-navigation-${_name}-color-darker) ${_STRIPE_WIDTH_FG}), linear-gradient(180deg, black, rgba(var(--top-navigation-${_name}-color-r), var(--top-navigation-${_name}-color-g), var(--top-navigation-${_name}-color-b), calc(256/256)));
}
EOF
done
