<?php
	$settings_base_mix = $_COOKIE;
	if (!empty($settings) && isset($settings)) $settings_base_mix = array_merge($settings_base_mix, $settings);
	if (!isset($genre) || is_null($genre)) $genre=$page;
	if ($settings_base_mix["useJapaneseInHeader"] === "true") {
		if (isset($title_ja) && !is_null($title_ja) && $title_ja != "") $title = $title_ja;
		if (isset($h1_ja) && !is_null($h1_ja) && $h1_ja != "") $h1 = $h1_ja;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title><?php echo htmlspecialchars($title, ENT_QUOTES, "UTF-8", true); ?></title>
		<link rel="stylesheet" type="text/css" href="/index.new.css">
		<script type="text/javascript" src="/index.new.js"></script>
	</head>
	<body class="body-<?php echo htmlspecialchars($genre, ENT_QUOTES, "UTF-8", true); ?>">
<?php
	$replace_forpc = [
		'/top-navigation-'.htmlspecialchars($page, ENT_QUOTES, "UTF-8", true).'" href.*?\/span>/' => '\0<span class="triangle"></span>',
		'/"top-navigation"/' => '"top-navigation pc-top-navigation"'
	];
	ob_start();
	include 'index.new.files/ismodern.php';
	if (ob_get_clean() == "true" || $settings_base_mix["theme"] === "stripe") {} else {
		$replace_forpc += array('/<img .*?>/' => '');
	}
	$topnav_filename = __DIR__ . '/topnav.html';
	if ($settings_base_mix["useJapaneseInHeader"] === "true") $topnav_filename = __DIR__ . '/topnav.ja.html';
	echo preg_replace(array_keys($replace_forpc), array_values($replace_forpc), file_get_contents($topnav_filename));
	//if ($settings_base_mix["theme"] !== "simplest" && $settings_base_mix["theme"] !== "simplest_mona" && $settings_base_mix["theme"] !== "classic_a") {
	if ($settings_base_mix["theme"] === "default" || $settings_base_mix["theme"] === "stripe" || !isset($settings_base_mix["theme"]) || is_null($settings_base_mix["theme"])) {
		ob_start();
		include 'index.new.files/ismodern.php';
		if (ob_get_clean() == "true" || $settings_base_mix["theme"] === "stripe") {
			echo "\t\t<details class=\"mobile-wrapper-top-navigation\">\n\t\t\t<summary>MENU</summary>\n\n\t";
			echo preg_replace('/\n/', "\n\t", preg_replace('/top-navigation-'.htmlspecialchars($page, ENT_QUOTES, "UTF-8", true).'" href.*?\/span>/', '\0<span class="triangle"></span>', file_get_contents($topnav_filename)));
			echo "\t</details>\n\t\t<div class=\"mobile-top-padding\"></div>\n";
			echo <<< 'EOD'
		<script>
			// @license https://creativecommons.org/licenses/zero/1.0/ CC0-1.0
			document.querySelector("details.mobile-wrapper-top-navigation > .top-navigation").addEventListener("click", () => {
				document.querySelector("details.mobile-wrapper-top-navigation").removeAttribute("open");
			});
			// @license-end
		</script>

EOD;
		}
	}
?>
<?php if (isset($h1) || !is_null($h1)) echo '		<h1>' . htmlspecialchars($h1, ENT_QUOTES, "UTF-8", true) . '</h1>' . "\n"; ?>
		<div class="main-content">
