<?php
	if (!isset($dest) || is_null($dest) || $dest == "") {
		http_response_code(500);
		include $_SERVER['DOCUMENT_ROOT'] . '/errordocument.php';
		die();
	}
	$dest = $_SERVER['DOCUMENT_ROOT'] . '/pages/' . $dest . '.xml';
	$xml = simplexml_load_file($dest);

	$title = $xml -> title;
	$title_ja = $xml -> {'title.ja'};
	$genre = $xml -> genre;
	$page = $xml -> pageId;
	$h1 = $xml -> h1;
	$h1_ja = $xml -> {'h1.ja'};
	include __DIR__ . '/base-mix.php';
	print("\t\t\t" . $xml -> data . "\n");
?>
		</div>
	</body>
</html>
