<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<style>
			.a {
			    color: blue;
			    cursor: pointer;
			    text-decoration: underline;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="main.css">
		<title>WELCOME!</title>
	</head>
	<body>
		<h1 style="margin: 0"><img src="curve2.svg" alt="WELCOME!" id="headerimg"></h1>
		<hr class="holoBlueLine">
		<ul style="">
			<li><a href="android-holo-colors/">android-holo-colors/</a> ソースコード(GitHub): <a href="https://github.com/nnn1590/android-holo-colors">https://github.com/nnn1590/android-holo-colors</a>
			<li><a href="https://nnn1590.org">https://nnn1590.org</a>
		</ul>
		<?php
		$ua = $_SERVER['HTTP_USER_AGENT'];
		echo "Your user agent is: " . $ua . "<br>";
		if((strpos($ua, 'GNU') !== false) || (strpos($ua, 'Linux') !== false) || (strpos($ua, 'Android') !== false) || (strpos($ua, 'BSD') !== false) || (strpos($ua, 'Haiku') !== false)) echo "GREAT!";
		?>
		<hr>
		This site using <a href="https://github.com/ZMYaro/holo-web">holo-web</a>; Copyright 2012-2015 Zachary Yaro / MIT/X11 License<br>
		Thanks!<br>
		<a href="https://ihavenoads.com/"><img src="https://ihavenoads.com/1.svg" width="45" height="56" alt="Ad-free site"/></a>
		<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="クリエイティブ・コモンズ・ライセンス" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />この 作品 は <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">クリエイティブ・コモンズ 表示 - 継承 4.0 国際 ライセンス</a>の下に提供されています。<br>
	</body>
</html>
