<?php
	$theme_array = [
		"default" => "ストライプ+超シンプル（デフォルト）",
		"stripe" => "ストライプ",
		"simplest" => "超シンプル",
		"simplest_mona" => "超シンプル - モナー",
		"simplest_dark" => "超シンプル(ダーク)",
		"simplest_dark_mona" => "超シンプル(ダーク) - モナー",
		//"classic_a" => "クラシック A（工事中）"
	];

	$post2 = array();
	foreach ($_POST as $key => $value) {
		$post2[htmlspecialchars($key, ENT_QUOTES)] = htmlspecialchars($value, ENT_QUOTES);
	}
	unset($key, $value);

	$settings = array(
		// 設定名 => デフォルト値,
		// 全部stringで！
		"useAjax" => "false",
		"useJapaneseInHeader" => "false",
		"useMonaFont" => "false",
		"theme" => "default",
		"useNightMode" => "false",
	);
	foreach ($settings as $key => $value) {
		if (!empty($_COOKIE[$key]) && isset($_COOKIE[$key])) {
			$settings[$key] = $_COOKIE[$key];
		}
		if (!empty($post2[$key]) && isset($post2[$key])) {
			if ($post2[$key] === "on") {
				$settings[$key] = "true";
				setcookie($key, "true");
			} elseif ($post2[$key] === "off") {
				$settings[$key] = "false";
				setcookie($key, "false");
			} else {
				$settings[$key] = $post2[$key];
				setcookie($key, $post2[$key]);
			}
		}
	}
	unset($key, $value);
	$reloading = false;
	if ($reloading) {
		echo <<<'EOF'
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="refresh" content="0; URL=">
	</head>
	<body>
		設定しました！
		再読み込みしています…<br>
		<a href="/settings.php">切り替わらない場合、ここをクリックしてください。</s>
	</body>
</html>
EOF;
		die();
	}
?>
<?php $title = "Settings - NNN1590.org"; $title_ja = "設定 - NNN1590.org"; $page = "settings"; $h1 = "Settings"; $h1_ja = "設定";include 'parts/base-mix.php'; ?>
			<h2>設定です。</h2>
			<ul class="star-list">
				<li>設定はCookieに保存されます。そのため、Cookieが利用できない もしくは無効化してある環境では設定は保存されません。
				<li>設定後にページを再読み込みしないと反映されないかも。
				<li><s>今の所JavaScriptがないと設定できません。</s> JS無くても設定できるようになりました。設定後は下部の「設定する」ボタンを押してね。<br>
				    ちなみにJavaScriptが有効だと設定するボタンを押さなくても設定されますので注意…
				<li>モナーフォント設定は端末にフォントがインストールされている必要があります！
				    （CSSでモナーフォントの指定をするだけなので）
			</ul>
			<noscript>
				<div style="border: 1px solid white; margin: 8px 0; padding: 8px;">
					JavaScriptが無効化されているか未対応みたいです。<br>
					(LibreJS等を利用していても表示されるかも)
				</div>
			</noscript>
			<hr>
			<form action="#" method="POST" enctype="multipart/form-data">
				<input name="useJapaneseInHeader" type="hidden" value="off"><!-- チェックされていないと$_POST[name]自体が存在しなくなるので代わりに送る値 -->
				<input name="useMonaFont" type="hidden" value="off">
				<label><input type="checkbox" name="useJapaneseInHeader" id="checkbox-toggle-japanese-in-header" <?php if ($settings["useJapaneseInHeader"] === "true") echo "checked";?>>ヘッダー等を日本語表記にする（可能な場合）</label><br>
				<label><input type="checkbox" name="useMonaFont" id="checkbox-toggle-mona-font" <?php if ($settings["useMonaFont"] === "true") echo "checked";?>>モナーフォントを有効化する</label><br>
				テーマ: <select name="theme" id="select-theme" title="テーマ">
<?php
						foreach ($theme_array as $theme_internal_name => $theme_display_name) {
							if ($settings["theme"] === $theme_internal_name) {
								$add = " selected";
							} else {
								$add = "";
							}
							echo "\t\t\t\t" . '<option value="' . $theme_internal_name . '"' . $add . '>' . htmlspecialchars($theme_display_name, ENT_QUOTES) . '</option>' . "\n";
						}
					?>
				</select>
				<br><?php //var_dump(get_browser(null, true)); ?>
				<dl>
					<dt>ストライプ+超シンプル（デフォルト）
					<dd>基本ストライプ、w3mやo3DS、IEには超シンプルテーマ(ダーク)を使う</dd>
					<dt>ストライプ
					<dd>モダンブラウザじゃないと崩れるかも</dd>
					<dt>超シンプル
					<dd>あまり飾り気のない、ものすごくシンプルなテーマ</dd>
					<dt>超シンプル(ダーク)
					<dd>超シンプルテーマの暗い版。背景色は#000なので有機ELディスプレイにもいいかも(文字色は#EEEです)</dd>
					<dt>超シンプル - モナー
					<dd>超シンプルテーマにモナーフォントの指定を入れたテーマ: <pre class="code">body {
	font-size: 16px;
	line-height: 18px;
	font-family: 'Monapo', 'Mona', 'IPAMonaPGothic', 'IPA モナー Pゴシック', 'Togoshi Mona Gothic', '戸越モナーゴシック', 'MS PGothic', 'ＭＳ Ｐゴシック', sans-serif;
}
</pre></dd>
					<dt>超シンプル(ダーク) - モナー
					<dd>超シンプルテーマのダークとモナーの合成版
					<!--（『このフォントがない！』ってのがあったらごめんね）-->
				</dl>
				<script>
					// @license https://creativecommons.org/licenses/zero/1.0/ CC0-1.0
<?php
	$js_declare_base_const = "const";
	$js_declare_base_let = "let";
	$addEventListener = "addEventListener";
	$event_listener_prefix = "";
	if (preg_match('/(?i)msie /', $_SERVER["HTTP_USER_AGENT"])) {
		$js_declare_base_const = "var";
		$js_declare_base_let = "var";
		$addEventListener = "attachEvent";
		$event_listener_prefix = "on";
		echo <<<'EOF'
				// IE10以下はconst使えないってこれマジ？しょうがねぇなぁ（悟空）
				// IE8以下はaddEventListenerも使えないのか（驚愕）
					var checkboxToggleJapaneseInHeader = document.getElementById("checkbox-toggle-japanese-in-header");

EOF;
	} else {
		echo <<<'EOF'
					const checkboxToggleJapaneseInHeader = document.getElementById("checkbox-toggle-japanese-in-header");

EOF;
	}
?>
					checkboxToggleJapaneseInHeader.<?php echo $addEventListener; ?>('<?php echo $event_listener_prefix; ?>change', function() {
						if (checkboxToggleJapaneseInHeader.checked) {
							document.cookie = "useJapaneseInHeader=true";
						} else {
							document.cookie = "useJapaneseInHeader=false";
						}
					});
					<?php echo $js_declare_base_const; ?> checkboxToggleMonaFont = document.getElementById("checkbox-toggle-mona-font");
					checkboxToggleMonaFont.<?php echo $addEventListener; ?>('<?php echo $event_listener_prefix; ?>change', function() {
						if (checkboxToggleMonaFont.checked) {
							document.cookie = "useMonaFont=true";
						} else {
							document.cookie = "useMonaFont=false";
						}
					});
					<?php echo $js_declare_base_const; ?> selectTheme = document.getElementById("select-theme");
					selectTheme.<?php echo $addEventListener; ?>('<?php echo $event_listener_prefix; ?>change', function() {
						document.cookie = "theme=" + selectTheme.value;
					});
					// @license-end
				</script>
				<input type="submit" value="設定する">
			</form>
			<hr>
		</div>
	</body>
</html>
